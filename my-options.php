<?php
/*
Plugin Name: Headway OPTIONS
Plugin URI: https://bicarbona:lucuska@bitbucket.org/bicarbona/headway-options
Description: Advanced OPTIONS 
Version: 1.0.3
Author: Etienne
Author URI: www.etienne.sk
License: GNU GPL v2
* Bitbucket Plugin URI: https://bicarbona:lucuska@bitbucket.org/bicarbona/headway-options
* Bitbucket Branch: master
*/


add_action('headway_register_elements', 'headway_test_register_elements', 50);


/**
 * Everything is ran at the after_setup_theme action to insure that all of Headway's classes and functions are loaded.
 **/
function headway_test_register_elements() {

	HeadwayElementAPI::register_element(array(
		'group' => 'blocks',
		'id' => 'block-widget-tabbertabs',
	//	'parent' => 'block-content',
		'name' => 'YEaaa Title',
		'description' => ' ',
		'selector' => ' ul.tabbernav li.tabberactive a'
	));

	HeadwayElementAPI::register_element(array(
		'group' => 'blocks',
		'id' => 'content-post-meta',
		'parent' => 'block-content',
		'name' => 'Post Meta',
		'description' => ' ',
		'selector' => '.post-meta-bar, .post-meta-bar a'
	));
	
	HeadwayElementAPI::register_element(array(
		'group' => 'blocks',
		'id' => 'content-post-meta-link',
		'parent' => 'block-content',
		'name' => 'Post Meta Link',
		'description' => ' ',
		'selector' => '.post-meta-bar a',
		'states' => array(
			'Hover' => '.post-meta-bar a:hover', 
			'Clicked' => '.post-meta-bar a:active'
		),
	));


/**

WOOCOMMERCE

**/	
	HeadwayElementAPI::register_element(array(
		'group' => 'blocks',
		'id' => 'woo-single-product-meta',
		'parent' => 'block-content',
		'name' => 'Product meta',
		'description' => 'Storefront: WooCommerce',
		'selector' => 'div.summary div.product_meta',
		'states' => array(
			'Hover' => 'div.summary div.product_meta:hover'
			),
	));

// Single Price

#product-2527 > div.summary.entry-summary > h2.alt_product_title

	HeadwayElementAPI::register_element(array(
		'group' => 'blocks',
		'id' => 'woo-product-single-alt-name',
		'parent' => 'block-content',
		'name' => 'Product Alt Name',
		'description' => 'Storefront: WooCommerce',
		'selector' => 'h2.alt_product_title'
	));


	HeadwayElementAPI::register_element(array(
		'group' => 'blocks',
		'id' => 'woo-product-single-price',
		'parent' => 'block-content',
		'name' => 'Product Price',
		'description' => 'Storefront: WooCommerce',
		'selector' => 'p.price',
		'states' => array(
			'Hover' => 'p.price:hover'
			),
	));


	HeadwayElementAPI::register_element(array(
		'group' => 'blocks',
		'id' => 'woo-product-single-description',
		'parent' => 'block-content',
		'name' => 'Product Description',
		'description' => 'Storefront: WooCommerce',
		'selector' => 'div[itemprop="description"] '
	));

	

// Price DEL

	HeadwayElementAPI::register_element(array(
		'group' => 'blocks',
		'id' => 'woo-product-price-del',
		'parent' => 'block-content',
		'name' => 'Product Price Del',
		'description' => 'Storefront: WooCommerce',
		'selector' => '.woocommerce ul.products li.product .price del, .woocommerce-page ul.products li.product .price del',
		'states' => array(
			'Hover' => '.woocommerce ul.products li.product .price del:hover, .woocommerce-page ul.products li.product .price del:hover'
			),
	));

	HeadwayElementAPI::register_element(array(
		'group' => 'blocks',
		'id' => 'woo-product-price',
		'parent' => 'block-content',
		'name' => 'Product Price',
		'description' => 'Storefront: WooCommerce',
		'selector' => '.woocommerce ul.products li.product .price ins, .woocommerce-page ul.products li.product .price ins',
		'states' => array(
			'Hover' => '.woocommerce ul.products li.product .price ins:hover, .woocommerce-page ul.products li.product .price ins:hover'
			),
	));

	HeadwayElementAPI::register_element(array(
		'group' => 'blocks',
		'id' => 'woo-tab-li',
		'parent' => 'block-content',
		'name' => 'Tab',
		'description' => 'Storefront: WooCommerce',
		'selector' => 'div.woocommerce-tabs ul.tabs li a',
		'states' => array(
			'Hover' => 'div.woocommerce-tabs ul.tabs li a:hover'
		),
	));

	HeadwayElementAPI::register_element(array(
		'group' => 'blocks',
		'id' => 'woo-tab-li-active',
		'parent' => 'block-content',
		'name' => 'Tab Active',
		'description' => 'Storefront: WooCommerce',
		'selector' => 'div.woocommerce-tabs ul.tabs li.active a',
		'states' => array(
			'Hover' => 'div.woocommerce-tabs ul.tabs li.active a:hover'
		),
	));

	HeadwayElementAPI::register_element(array(
		'group' => 'blocks',
		'id' => 'woo-block-content-block-hover-onsale',
		'parent' => 'block-content',
		'name' => 'On sale',
		'description' => 'Storefront: WooCommerce',
		'selector' => '.woocommerce .products .product span.onsale',
		'states' => array(
			'Hover' => '.woocommerce .products .product:hover span.onsale'
			),
	));


// BUY ONLINE

HeadwayElementAPI::register_element(array(
		'group' => 'blocks',
		'id' => 'woo-buy-button',
		'parent' => 'block-content',
		'name' => 'Buy Online',
		'description' => 'Storefront: WooCommerce',
		'selector' => '
		.woocommerce #content input.button.alt,
		.woocommerce #respond input#submit.alt,
		.woocommerce a.button,
		.woocommerce #content button.button,
		.woocommerce a.button.alt,
		.woocommerce button.button.alt,
		.woocommerce input.button.alt,
		.woocommerce-page #content input.button.alt,
		.woocommerce-page #respond input#submit.alt,
		.woocommerce-page a.button.alt,
		.woocommerce-page button.button.alt,
		.woocommerce-page input.button.alt,
		.woocommerce ul.products li a.added_to_cart,
		.woocommerce a.button,
		.woocommerce button.button,
		.woocommerce input.button, 
		.woocommerce #respond input#submit,
		.woocommerce #content input.button,
		.woocommerce-page a.button,
		.woocommerce-page button.button,
		.woocommerce-page input.button,
		.woocommerce-page #respond input#submit,
		.woocommerce-page #content input.button
		.woocommerce ul.products li a.added_to_cart,
		.woocommerce a.button,
		.woocommerce button.button,
		.woocommerce input.button,
		.woocommerce #respond input#submit,
		.woocommerce #content input.button,
		.woocommerce-page a.button,
		.woocommerce-page button.button,
		.woocommerce-page input.button,
		.woocommerce-page #respond input#submit,
		.woocommerce-page #content input.button
		',
						
		'states' => array(
			'Hover' => '
		.woocommerce #content input.button.alt:hover,
		.woocommerce #respond input#submit.alt:hover,
		.woocommerce a.button:hover,
		.woocommerce #content button.button:hover,
		.woocommerce a.button.alt:hover,
		.woocommerce button.button.alt:hover,
		.woocommerce input.button.alt:hover,
		.woocommerce-page #content input.button.alt:hover,
		.woocommerce-page #respond input#submit.alt:hover,
		.woocommerce-page a.button.alt:hover,
		.woocommerce-page button.button.alt:hover,
		.woocommerce-page input.button.alt:hover,
		.woocommerce ul.products li a.added_to_cart:hover,
		.woocommerce a.button:hover,
		.woocommerce button.button:hover,
		.woocommerce input.button:hover, 
		.woocommerce #respond input#submit:hover,
		.woocommerce #content input.button:hover,
		.woocommerce-page a.button:hover,
		.woocommerce-page button.button:hover,
		.woocommerce-page input.button:hover,
		.woocommerce-page #respond input#submit:hover,
		.woocommerce-page #content input.button:hover
		.woocommerce ul.products li a.added_to_cart:hover,
		.woocommerce a.button:hover,
		.woocommerce button.button:hover,
		.woocommerce input.button:hover,
		.woocommerce #respond input#submit:hover,
		.woocommerce #content input.button:hover,
		.woocommerce-page a.button:hover,
		.woocommerce-page button.button:hover,
		.woocommerce-page input.button:hover,
		.woocommerce-page #respond input#submit:hover,
		.woocommerce-page #content input.button:hover',
		),
	));

	HeadwayElementAPI::register_element(array(
		'group' => 'blocks',
		'id' => 'woo-widget-thumbnail',
		'parent' => 'block-content',
		'name' => 'Widget Thumbnail',
		'description' => 'Storefront: WooCommerce',
		'selector' => '.woocommerce ul.cart_list li img,
		 .woocommerce ul.product_list_widget li img,
		  .woocommerce-page ul.cart_list li img,
		   .woocommerce-page ul.product_list_widget li img',			
		'states' => array(
			'Hover' => '.woocommerce a.button:hover'
		),
	));

	HeadwayElementAPI::register_element(array(
		'group' => 'blocks',
		'id' => 'woo-tab-description-h2',
		'parent' => 'block-content',
		'name' => 'Tab description',
		'description' => 'Storefront: WooCommerce',
		'selector' => 'div.woocommerce-tabs div#tab-description.panel h2',
		'states' => array(
			'Hover' => 'div.woocommerce-tabs div#tab-description.panel h2:hover'
			),
	));

	HeadwayElementAPI::register_element(array(
		'group' => 'blocks',
		'id' => 'woo-widget-thumbnail',
		'parent' => 'block-content',
		'name' => 'Widget Thumbnail',
		'description' => 'Storefront: WooCommerce',
		'selector' => '.woocommerce ul.cart_list li img,
		 .woocommerce ul.product_list_widget li img,
		 .woocommerce-page ul.cart_list li img,
		 .woocommerce-page ul.product_list_widget li img',			
		'states' => array(
			'Hover' => '.woocommerce a.button:hover'
		),
	));

/**

SUBPAGES

**/

	HeadwayElementAPI::register_element(array(
		'group' => 'blocks',
		'id' => 'ul-subpages',
		'parent' => 'block-content',
		'name' => '# Subpages UL',
		'description' => '&lt;UL&gt;',
		'properties' => array('fonts', 'lists', 'background', 'borders', 'padding', 'corners', 'box-shadow'),
		'selector' => 'li.widget ul.subpages',
		
		//'selector' => 'ul .subpages',
		// 'states' => array(
		// 	'Hover' => 'div.woocommerce-tabs div#tab-description.panel h2:hover'
		// 	),
	));


	HeadwayElementAPI::register_element(array(
		'group' => 'blocks',
		'id' => ' ul-subpages-li',
		'parent' => 'ul-subpages',
		'name' => 'Subpages LI',
		'properties' => array('fonts', 'background', 'borders', 'padding', 'corners', 'box-shadow'),
		'description' => '&lt;LI&gt;',
		'selector' => 'li.widget ul .subpages li',
		// 'selector' => ' li ul .subpages li',
		// 'states' => array(
		// 	'Hover' => 'div.woocommerce-tabs div#tab-description.panel h2:hover'
		// 	),
	));


	HeadwayElementAPI::register_element(array(
		'group' => 'blocks',
		'id' => ' ul-subpages-li-a',
		'parent' => 'ul-subpages',
		'name' => 'Subpages Link',
		'properties' => array('fonts', '', 'background', 'borders', 'padding', 'corners', 'box-shadow'),
		//'description' => 'a',
		'selector' => ' ul .subpages li a',
		'states' => array(
			'Hover' => 'ul .subpages li a:hover'
			),
	));


	HeadwayElementAPI::register_element(array(
		'group' => 'blocks',
		'id' => ' ul-subpages-li-a-current',
		'parent' => 'ul-subpages',
		'name' => 'Subpages Link Current',
		'properties' => array('fonts', '', 'background', 'borders', 'padding', 'corners', 'box-shadow'),
		//'description' => 'a',
	
		'selector' => 'ul .subpages li.current_page_item a',
		'states' => array(
			'Hover' => 'ul .subpages li.current_page_item a:hover'
			),
	));



	
	HeadwayElementAPI::register_element(array(
		'group' => 'blocks',
		'id' => 'content-post-meta-retweet',
		'parent' => 'block-content',
		'name' => 'Post Meta Update',
		'description' => ' ',
		'selector' => ' div.post-meta-bar > i.fa.fa-retweet',
		// 'states' => array(
		// 	'Hover' => '.post-meta-bar a:hover', 
		// 	'Clicked' => '.post-meta-bar a:active'
		// ),
	));
	#post-4823 > div.post-meta-bar > i.fa.fa-retweet

}